QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_tests_array.cpp \
    main.cpp

SOURCES += ../functions.cpp

HEADERS += \
    tests.h

HEADERS += ../functions.h
