#ifndef TESTS_H
#define TESTS_H
#include <QtTest/QtTest>
// add necessary includes here

class tests_array : public QObject
{
    Q_OBJECT

public:



private slots:
    void test_index();
    void test_elements();
    void test_sort();
    void test_sequence();
    void test_max();
    void test_compare();
    void test_replacementByZero();
    void test_replacementByMaximum();
};

#endif // TESTS_H
