#include "tests.h"
#include <QtTest>
#include <../functions.h>
bool compareArray(int* array, int* array2)
{
    for (int i = 0; i < 3; i++)
    {
        if(array[i] != array2[i])
        {
            return false;
        }

    }
    return true;
}

void tests_array::test_index()
{
    int* array = new int[3];
    array[0] = 7;
    array[1] = 4;
    array[2] = 9;
    QCOMPARE(1,Index(array, 3));
    delete [] array;
}


void tests_array::test_elements()
{
    int* array = new int[3];
    array[0] = 7;
    array[1] = 4;
    array[2] = 9;
    QCOMPARE(1,Element(array, 3, 4));
    delete [] array;
}


void tests_array::test_sort()
{
    int* array = new int[3];
    array[0] = 4;
    array[1] = 1;
    array[2] = 7;
    int* array2 = new int[3];
    array2[0] = 1;
    array2[1] = 4;
    array2[2] = 7;
    SortU(array,3);
    QCOMPARE(true, compareArray(array,array2));

    int* array3 = new int[3];
    array3[0] = 7;
    array3[1] = 4;
    array3[2] = 1;
    SortD(array,3);
    QCOMPARE(true, compareArray(array,array3));
    delete [] array;
    delete [] array2;
    delete [] array3;
}

void tests_array::test_sequence()
{
    int* array = new int[3];
    array[0] = 9;
    array[1] = 3;
    array[2] = 3;
    QCOMPARE(2,Sequence(array, 3));
    delete [] array;
}

void tests_array::test_max()
{
    int* array = new int[5];
    array[0] = 4;
    array[1] = 1;
    array[2] = 7;
    array[3] = 8;
    array[4] = 0;
    int* array2 = new int[3];
    array2[0] = 8;
    array2[1] = 7;
    array2[2] = 4;
    Max(array,array2, 5);
    QCOMPARE(true, compareArray(array,array2));
}

void tests_array::test_compare()
{
    //�����������2
    int* array = new int[5];
    array[0] = 4;
    array[1] = 1;
    array[2] = 7;
    array[3] = 5;
    array[4] = 8;
    int* array2 = new int[5];
    array2[0] = 4;
    array2[1] = 1;
    array2[2] = 7;
    array2[3] = 5;
    array2[4] = 8;
    QCOMPARE(true,Compare(array,array2,5,5));
}

void tests_array::test_replacementByZero()
{
    int* array = new int[3];
    array[0] = -4;
    array[1] = 1;
    array[2] = -7;
    int* array2 = new int[3];
    array2[0] = 0;
    array2[1] = 1;
    array2[2] = 0;
    ReplacementByZero(array,3);
    QCOMPARE(true, compareArray(array,array2));
    delete [] array;
    delete [] array2;
}

void tests_array::test_replacementByMaximum()
{
    int* array = new int[3];
    array[0] = 4;
    array[1] = 1;
    array[2] = 7;
    int* array2 = new int[3];
    array2[0] = 7;
    array2[1] = 7;
    array2[2] = 7;
    ReplacementByMaximum(array,3);
    QCOMPARE(true, compareArray(array,array2));
    delete [] array;
    delete [] array2;
}
