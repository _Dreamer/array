#ifndef FUNCTIONS_H
#define FUNCTIONS_H
/*!
\file
\brief Head file with description of all functions in project

This file holds defenitions of all functions that are used in project
*/
int Index (int* array, int size);
int Element(int* array, int size, int element);
void SortU(int* array, int size);
void SortD(int* array, int size);
int Sequence (int* aray, int size);
void Max(int* array, int* max, int size);
bool Compare (int* array, int* _array, int size, int _size);
void ReplacementByZero(int* array, int size);
void ReplacementByMaximum(int* array, int size);
#endif // FUNCTIONS_H
