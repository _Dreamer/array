#include "functions.h"
#include "iostream"
using namespace std;

/**
 * @brief Function to find the minimum element
 * @param array
 * @param size
 * @return index
 */

int Index (int* array, int size)
{
    int check = 0;
    int index = 0;
    check = array[0];
    for (int i = 0;i < size; i++)
    {
        if(check > array[i])
        {
            check = array[i];
            index = i;
        }
    }
    return index;
}

/**
 * @brief Index of element
 * @param array
 * @param size
 * @param element
 * @return index
 */
int Element(int* array, int size, int element)
{
    int index = -1;
    for (int i = 0; i < size; i++)
    {
        if(array[i] == element)
        {
            index = i;
            cout << index << " ";
        }

    }
    return index;
}

/**
 * @brief Sort array ascending
 * @param array
 * @param size
 */
void SortU(int* array, int size)
{
    int sort = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if(array[j] > array [j+1])
            {
                sort = array[j];
                array[j] = array[j+1];
                array[j+1] = sort;
            }
        }
    }
}

/**
 * @brief Sort array descending
 * @param array
 * @param size
 */
void SortD(int* array, int size)
{
    int sort = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if(array[j] < array [j+1])
            {
                sort = array[j];
                array[j] = array[j+1];
                array[j+1] = sort;
            }
        }
    }
}

/**
 * @brief Function to find the longest sequence of identical elements
 * @param array
 * @param size
 * @return compare
 */
int Sequence (int* array, int size)
{
    int compare = 1;
    int count = 1;
    for (int i = 0; i < size - 1; i++)
    {
        if(array[i] == array[i+1])
        {
            count++;
            if(count > compare)
            {
                compare = count;
            }
        }
        else
        {

            count = 1;
        }

    }
    return compare;
}

/**
 * @brief Function to find the three maximum elements
 * @param array
 * @param max
 * @param size
 */
void Max(int* array, int* max, int size)
{

    int sort = 0;//�����������1
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size-1; j++)
        {
            if(array[j] < array [j+1])
            {
                sort = array[j];
                array[j] = array[j+1];
                array[j+1] = sort;
            }
        }
    }
    for (int i = 0; i < 3; i++)
    {
        max[i] = array[i];
    }

}

/**
 * @brief Comparing 2 arrays
 * @param array
 * @param _array
 * @param size
 * @param _size
 * @return compare
 */
bool Compare(int* array, int* _array, int size, int _size)
{
    //bool compare = true;
    if (size == _size)
    {
        for (int i = 0; i < size; i++)
        {
            if(array[i] != _array[i])
            {
                return false;
            }
        }
    }

    else
    {
        return false;
    }
    return true;
}

void ReplacementByZero(int* array, int size)
{
    for (int i = 0; i < size; i++)
    {
            if(array[i] < 0)
            {
                array[i] = 0;
            }
    }

}

void ReplacementByMaximum(int* array, int size)
{
    int max = array[0];
    for (int i = 1; i < size; i++)
    {
         if(array[i] > max)
          max = array[i];
    }
    for (int i = 0; i < size; i++)
    {
         array[i] = max;
    }
}
